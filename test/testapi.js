var mocha = require ('mocha')
var chai = require('chai')
var chaiHttp = require ('chai-http')
var server = require('../server')
var cors = require('cors')
var should = chai.should()

chai.use(chaiHttp) //configurar chai con módulo HTTP


describe ('Test de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request ('http://www.google.es')
      .get('/')
      .end((err, res) => {
        res.should.have.status(200)
        done()
      })
  })

})

describe ('Test de API usuarios', () => {
  it('Lista de usuarios', (done) => {
    chai.request ('http://localhost:3000')
      .get('/apitechu/v5/usuarios')
      .end((err,res) => {
        res.should.have.status(200)
        res.body.should.be.a('array')
        for (var i = 0; i < res.body.length; i++){
          res.body[i].should.have.property('email')
          res.body[i].should.have.property('pasword')
        }
        done()
      })
  })
})
