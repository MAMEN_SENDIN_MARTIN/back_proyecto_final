var express = require('express');
var bodyParser = require ('body-parser')
var app = express();
app.use(bodyParser.json())
//var cors = require('cors')

var requestJson =require ('request-json')

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancomse/collections"
var apiKey = "apiKey=ji18FLP6u_rVYXMetkn3TcRrsoTgiyqJ"
var clienteMlab


//API para test. Listado de usuarios

app.get('/apitechu/v5/usuarios', function(req,res){
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?"+ apiKey)

  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      res.send(body)
    }
  })

})



// Log in dado email y password y registro de que se ha quedado logado (actualizar datos)

app.post('/apitechu/v5/login', function(req,res){
  var email = req.headers.email
  var pasword = req. headers.pasword
  var query = 'q={"email":"' + email + '", "pasword":"' + pasword + '"}'
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)

  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      if (body.length ==1) //log in ok
      {
        clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put('?q={"id":'+ body [0].id + '}&' + apiKey, JSON.parse(cambio), function (errP, resP, bodyP){
        res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
      })
    }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})


//Hacer Log out dado un id de usuario (marcando que ha quedado deslogado)

app.post('/apitechu/v5/logout', function(req,res){
  var id = req.headers.id
  var query = 'q={"id":' + id + ', "logged":true}'
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)
  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      if (body.length ==1) // estaba logado
      {
        clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":false}}'
        clienteMlab.put('?q={"id":'+ body [0].id + '}&' + apiKey, JSON.parse(cambio), function (errP, resP, bodyP){
        res.send({"logout":"ok", "id":body[0].id})
      })
    }
      else {
        res.status(200).send('Usuario no logado previamente')
      }
    }
  })
})


//dado un id de un usuario, obtener las cuentas y el saldo

app.get('/apitechu/v5/cuentas', function (req, res)
{
var idcliente = req.headers.idcliente
var query = 'q={"idcliente":' + idcliente + '}'
var filter = 'f={"iban":1,"saldo":1,"_id":0}'
clienteMlab = requestJson.createClient (urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
clienteMlab.get('', function(err, resM, body){
  if (!err) {
  res.send(body)
    }
})
})


// dado una cuenta de una cuenta, obtener los movimientos de la cuenta:

app.get('/apitechu/v5/movimientos', function(req, res) {
  var iban = req.headers.iban
  var query = 'q={"iban": "' + iban + '"}'
  var filter = 'f={"movimientos":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
  clienteMlab.get('', function(err, resM, bodyP) {
  if (!err) {
      // console.log(clienteMlab)
        res.send(bodyP)
      //  console.log("bodyP: ", bodyP)
      }
      else {
        res.status(404).send("iban no encontrado")
      }
  })
})


//registro de nuevo usuario

app.post('/apitechu/v5/registro', function(req, res){

  var email = req.headers.email
  var pasword = req. headers.pasword
  var nombre= req.headers.nombre
  var apellido = req.headers.apellido
  var telefono = req.headers.telefono
  var ciudad = req.headers.ciudad
  var nif = req.headers.nif
  var nuevoRegistro = {}

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.get ('', function(err, resM, body) {
      if (!err) {
        var totalregistros = body.length
      //  console.log("totalregistros: ", totalregistros)
        var query = 'q={"nif":"' + nif + '"}'
      //  console.log("-" + urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey + "-");
        clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)
        clienteMlab.get('', function(err, resM, body){
      //    console.log("body tras select: ",body)
        //  console.log("body.length: ", body.length)
          if (!err) {
            if (body.length ==1) //el nif ya existe
            {
              res.send ({"Detalle":'Usuario ya está registrado'})
            }
          else {
            //console.log("entra alta del nuevo registro")
            nuevoRegistro.id = totalregistros + 1
            nuevoRegistro.nombre = nombre
            nuevoRegistro.apellido = apellido
            nuevoRegistro.email = email
            nuevoRegistro.pasword = pasword
            nuevoRegistro.ciudad = ciudad
            nuevoRegistro.telefono = telefono
            nuevoRegistro.nif = nif

            clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
            console.log("nuevoRegistro: ", nuevoRegistro)
            var cambio = JSON.stringify(nuevoRegistro)
          //  console.log("cambio: ", cambio)
            //console.log("clienteMlab: ", clienteMlab)
              clienteMlab.post('', JSON.parse(cambio),function(errP, resP, bodyP){
              console.log("bodyP: ", bodyP)
                      if (!errP) {
                        res.send({"Detalle":"Usuario registrado correctamente. Es necesario logarse nuevamente"})
                      }
                      else {
                        res.send({"Detalle":"Error al insertar datos de alta registro"})
                            }
                        })
                }
              }
                    })
                  }
                })
              })


//hacer traspaso /transferencia entre cuentas (alta nuevo movimiento y actualización de saldo)

app.post('/apitechu/v5/altamovimiento', function(req, res) {
  var iban = req.headers.iban
  var saldo =req.headers.saldo
  var importe2 = req.headers.importe
  var movimientonuevo =  {}

  var query = 'q={"iban":"' + iban + '"}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      var numeromovimientos = body[0].movimientos.length
      console.log(numeromovimientos)
      movimientonuevo.id = numeromovimientos + 1
      movimientonuevo.fecha = req.headers.fecha
      movimientonuevo.importe = importe2 * -1
      movimientonuevo.moneda = "EUR"
      movimientonuevo.Tipo = "Transferencia entre cuentas"
      movimientonuevo.cuentadestino = req.headers.cuentadestino
      console.log("movimientonuevo:", movimientonuevo)

      objeto_movimientos = body[0].movimientos
      console.log("objeto_movimientos 1:", objeto_movimientos)

            objeto_movimientos[numeromovimientos] = movimientonuevo
            console.log("objeto_movimientos 2:", objeto_movimientos)

            importenuevo = parseInt(movimientonuevo.importe)
            nuevo_saldo = saldo - importenuevo * -1
            var cambio = '{"$set":{"movimientos":' + JSON.stringify(objeto_movimientos) + '}}'
            console.log("cambio movimientos:", cambio)

            clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
              if (!errP){
                console.log("bodyP de movimientos: ", bodyP)
                var cambio = '{"$set":{"saldo":' + nuevo_saldo + '}}'
                console.log("cambio saldo:", cambio)
                clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
                  if (!errP){
                      res.send({"Operacion":"Actualizacion saldo","Detalle":"Transferencia realizada satisfactoriamente"})
                  }else {
                      res.send({"Operacion":"Actualizacion saldo","Detalle":"Fallo al ejecutar la Transferencia. Por favor, inténtelo de nuevo"})
                  }
                })
              }else{
                res.send({"Operación":"Transferencia","Detalle":"Fallo al ejecutar la Transferencia. Por favor, inténtelo de nuevo"})
              }
            })
          }
          else {
            res.send({"operacion":"Recuperar operacion","Detalle":"Fallo en la operación de Transferencia. Por favor, inténtelo de nuevo"})
          }
        })
      })


      // Dado un id de usuario obtener los datos del usuario

      app.get('/apitechu/v5/datosusuario', function(req,res){
        var id = req.headers.id
        var query = 'q={"id":' + id + '}'
        clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)

        clienteMlab.get('', function(err, resM, bodyP){
          if (!err) {
            res.send(bodyP)
          }
            else {
              res.status(404).send('Usuario no encontrado')
            }
        })
      })



      //Dado un id de usuario modificar su password

      app.post('/apitechu/v5/modificarpassword', function(req, res) {
        var id = req.headers.id
        var pasword =req.headers.pasword
        var nuevapassword =req.headers.nuevapassword
        var query = 'q={"id":' + id + '}'
        clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
          clienteMlab.get('', function(err, resM, body){
            if (!err) {
              if (body.length ==1) //existe el id
              console.log(body.leght)
                  {
                clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios")
                var cambio = '{"$set":{"pasword":' + JSON.stringify(nuevapassword) + '}}'
                clienteMlab.put('?q={"id":'+ body [0].id + '}&' + apiKey, JSON.parse(cambio), function (errP, resP, bodyP){
                res.send({"Resultado":"Cambio de contraseña corrrecta", "id":body[0].id})
              })
            }
              }
              })
            })


var port = process.env.PORT || 3000;
var fs = require('fs')
app.listen(port);
console.log("API escuchando en el puerto"+ port);
